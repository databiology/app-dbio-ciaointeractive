
library('ggplot2')
library("ggpubr")

mytable <- '/scratch/table_out.tsv'

server <- function(input, output) {
  
  output$myexampleplot <- renderPlot({
    ta <- read.table(mytable, sep = '\t', header = TRUE)

    if (as.numeric(input$selectType) == 1) {
      ggdensity(ta, x = "Age",
        add = "mean", rug = TRUE,
        color = "Sex", fill = "Sex",
        palette = c("#00AFBB", "#E7B800"))
    } else if (as.numeric(input$selectType) == 2) {
      gghistogram(ta, x = "Age",
        add = "mean", rug = TRUE,
        color = "Sex", fill = "Sex",
        palette = c("#00AFBB", "#E7B800"))
    } else if (as.numeric(input$selectType) == 3) {
      ggboxplot(ta, x = "Sex", y = "Age",
        color = "Sex", palette =c("#00AFBB", "#E7B800", "#FC4E07"),
        add = "jitter", shape = "Sex")
    } else {
      ggviolin(ta, x = "Sex", y = "Age", fill = "Sex",
        palette = c("#00AFBB", "#E7B800", "#FC4E07"),
        add = "boxplot", add.params = list(fill = "white"))
    }
  })
}