#!/bin/bash
set -euo pipefail

# CIAO Interactive Example App Entry Point
# Carlos De Blas <carlos.deblas@databiology.com>
# (C) Databiology 2019


# SCRATCH=/scratch
# RESULTDIR=/scratch/results
# WORKDIR=/scratch/work

# Functions
falseexit () {
   echo "Application failed: $1"
   exit 1
}

SCRATCH=/scratch
SOURCEDIR=${SCRATCH}/input
RESULTDIR=${SCRATCH}/results
WORKDIR=${SCRATCH}/work

# Create workdir
if [ -d $WORKDIR ]; then
    echo "Using $WORKDIR as work directory"
else 
    mkdir $WORKDIR
fi

chown root /scratch \
    /scratch/reports \
    /scratch/metadata \
    /scratch/results \
    /scratch/work

python /usr/local/bin/json2tsv.py

mkdir -p /var/log/shiny-server
chown root.root /var/log/shiny-server
# If Shiny Server is running without root privileges,
# then dbe must have read/write access to this directory  
chown root.root /var/lib/shiny-server

gosu root shiny-server > ${RESULTDIR}/shiny-server-log.txt 2>&1 &

mitmdump -q --mode reverse:http://127.0.0.1:8000 -p 8081 -s /opt/databiology/apps/capturer.py &

cd /opt/databiology/apps/dbe/ && /opt/databiology/apps/shiny/serve &

echo "Running shiny-server..."

while true; do
    sleep 10
    if [ -e "/scratch/done" ]; then
        break
    fi
done