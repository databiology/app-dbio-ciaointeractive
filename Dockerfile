FROM app/dbio/shiny:1.1.0

RUN python -m pip install --upgrade pip setuptools wheel && \
    pip install pandas

RUN R -e "install.packages(c('ggplot2', 'ggpubr'))"

COPY server.R  /srv/shiny-server/
COPY ui.R /srv/shiny-server/

COPY main.sh /usr/local/bin
RUN chmod a+x /usr/local/bin/main.sh

COPY json2tsv.py /usr/local/bin
RUN chmod a+x /usr/local/bin/json2tsv.py

