import json, csv
import pandas as pd

jsonfile = '/scratch/projection.json'
outputfile = '/scratch/table_out.tsv'
head = ['Age', 'Sex']

with open(jsonfile) as fh:
    try:
        data = json.load(fh)
    except Exception as e:
        print (e)

rows = []
for resource in data['subjects']:
    if 'entityTypeAttributes' in resource:
        age = resource['entityTypeAttributes']['Age']
        sex = resource['entityTypeAttributes']['Sex']['name']

        row = []

        if age:
            row.append(age)

        if sex:
            row.append(sex)

    if len(row) != 0:
        rows.append(row)

if len(rows) != 0:
    df = pd.DataFrame(data=rows, columns=head)
    df.to_csv(outputfile, sep='\t', na_rep='', header=True, index=False)
else:
    print('error: there is no match data')
    exit(1)
        
                         